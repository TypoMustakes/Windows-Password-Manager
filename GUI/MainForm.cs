using Common;
namespace GUI;

sealed public partial class MainForm : Form
{
    public event DataRequest PathRequest;
	public event DataRequest? RecipientRequest;

    public MainForm(DataRequest PathRequest, DataRequest? RecipientRequest = null)
    {
        this.PathRequest = PathRequest;
		this.RecipientRequest = RecipientRequest;
        InitializeComponent();

        ResultList.SearchQueryRequest += () => SearchBox.Text;
        ResultList.ReloadResults();
    }

    private void ReloadResults(object? sender, EventArgs? e) //proxy method for SearchBox.TextChanged
    {
        ResultList.ReloadResults();
    }

    private void ReloadResults()
    {
        ResultList.ReloadResults();
    }

    private void OpenPasswordGenerator(object sender, EventArgs e)
    {
		if (RecipientRequest != null)
		{
			GeneratePassword gp = new GeneratePassword(PathRequest(), RecipientRequest(), ReloadResults, SearchBox.Text);
			gp.ShowDialog();
		}
		else
		{
			throw new InvalidOperationException("You cannot use the OpenPasswordGenerator method if you instantiated this form without a RecipientRequest event handler.");
		}
    }

    private void CancelPressed(object sender, EventArgs e)
    {
        Close();
    }

	private void CopyAndNotify(string? line, string fileName)
	{
        ProcessBuilder pb = new ProcessBuilder();
        pb.ProcessFailed += (e) => MessageBox.Show(e.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        if (line != null)
        {
            Clipboard.SetText(line);
            pb.Run("./ToastNotification.exe", $"\"{fileName} decrypted\" \"Password copied to clipboard\"");
        }
        else
        {
            pb.Run("./ToastNotification.exe", "\"Error\" \"No password copied\"");
        }
	}
		
    private void Decrypt(object sender, EventArgs e)
    {
		if (ResultList.Text != null)
		{
			ProcessBuilder pb = new ProcessBuilder();
			pb.ProcessFailed += (e) => MessageBox.Show(e.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			CopyAndNotify(pb.GetOutput("gpg.exe", $"--quiet --decrypt {PathRequest()}\\{ResultList.Text}"), ResultList.Text);
			Close();
		}
    }
}
