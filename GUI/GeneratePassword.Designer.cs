﻿namespace GUI;

partial class GeneratePassword
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        passwordName = new TextBox();
        label1 = new Label();
        label2 = new Label();
        passwordLength = new TextBox();
        noSymbols = new CheckBox();
        generate = new Button();
        cancel = new Button();
        SuspendLayout();
        // 
        // passwordName
        // 
        passwordName.Location = new Point(12, 33);
        passwordName.Name = "passwordName";
        passwordName.PlaceholderText = "website.com";
        passwordName.Size = new Size(156, 23);
        passwordName.TabIndex = 0;
        // 
        // label1
        // 
        label1.AutoSize = true;
        label1.Location = new Point(12, 15);
        label1.Name = "label1";
        label1.Size = new Size(39, 15);
        label1.TabIndex = 1;
        label1.Text = "Name";
        // 
        // label2
        // 
        label2.AutoSize = true;
        label2.Location = new Point(12, 59);
        label2.Name = "label2";
        label2.Size = new Size(44, 15);
        label2.TabIndex = 2;
        label2.Text = "Length";
        // 
        // passwordLength
        // 
        passwordLength.Location = new Point(12, 77);
        passwordLength.Name = "passwordLength";
        passwordLength.PlaceholderText = PasswordGenerator.DEFAULT_LENGTH;
        passwordLength.Size = new Size(156, 23);
        passwordLength.TabIndex = 3;
        // 
        // noSymbols
        // 
        noSymbols.AutoSize = true;
        noSymbols.Location = new Point(12, 106);
        noSymbols.Name = "noSymbols";
        noSymbols.Size = new Size(89, 19);
        noSymbols.TabIndex = 4;
        noSymbols.Text = "No symbols";
        noSymbols.UseVisualStyleBackColor = true;
        // 
        // generate
        // 
        generate.Location = new Point(93, 131);
        generate.Name = "generate";
        generate.Size = new Size(75, 23);
        generate.TabIndex = 5;
        generate.Text = "Generate";
        generate.UseVisualStyleBackColor = true;
        generate.Click += Generate;
        // 
        // cancel
        // 
        cancel.Location = new Point(12, 131);
        cancel.Name = "cancel";
        cancel.Size = new Size(75, 23);
        cancel.TabIndex = 6;
        cancel.Text = "Cancel";
        cancel.UseVisualStyleBackColor = true;
        cancel.Click += cancel_Click;
        // 
        // GeneratePassword
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(187, 173);
        Controls.Add(cancel);
        Controls.Add(generate);
        Controls.Add(noSymbols);
        Controls.Add(passwordLength);
        Controls.Add(label2);
        Controls.Add(label1);
        Controls.Add(passwordName);
        Name = "GeneratePassword";
        ResumeLayout(false);
        PerformLayout();
    }

    private void Generate_Click(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    #endregion

    private TextBox passwordName;
    private Label label1;
    private Label label2;
    private TextBox passwordLength;
    private CheckBox noSymbols;
    private Button generate;
    private Button cancel;
}
