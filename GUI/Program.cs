using Common;
namespace GUI;

public delegate string DataRequest(); //Fire whenever a specific field of ProfileHandler.CurrentProfile is needed
internal static class Program
{
    /// <summary>
    ///  The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
		ConfigFileManager.Init();
        ApplicationConfiguration.Initialize();
        Application.Run(mainForm);
    }

    public static MainForm mainForm = new MainForm(
        () => ConfigFileManager.GetPath(),
		() => ConfigFileManager.GetRecipient()
        ); //needed at creation so that MainForm may pass this method down to other classes in its constructor
}
