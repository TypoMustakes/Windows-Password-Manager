﻿using Common;
﻿namespace GUI;

public delegate void MethodRequest();
public partial class GeneratePassword : Form
{
    private string currentPath;
    private string recipient;
    public event MethodRequest ReloadRequest;

    public GeneratePassword(string currentPath, string recipient, MethodRequest ReloadRequest, string? name)
    {
        InitializeComponent();
        passwordName.Text = name;
        this.currentPath = currentPath;
        this.recipient = recipient;
        this.ReloadRequest = ReloadRequest;
    }

    public void Generate(object sender, EventArgs e)
    {
        if (passwordName.Text == "")
        {
            MessageBox.Show("You must provide a name to continue.", "Error: Empty field", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        else
        {

            try
            {
				int length;
				if (!int.TryParse(passwordLength.Text, out length))
				{
					length = PasswordGenerator.DEFAULT_LENGTH
				}
				
                PasswordGenerator.ExceptionOccured += (e) => MessageBox.Show(e.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                File.WriteAllText(
                    currentPath + $"\\{passwordName.Text}.gpg",
                    PasswordGenerator.New(
                        recipient,
                        length,
                        noSymbols.Checked)
                    );
            }
            catch (FormatException)
            {
                MessageBox.Show("Please enter a valid number for the password's length.", "Error: Invalid field", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IOException error)
            {
                MessageBox.Show(error.ToString(), "IO Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        ReloadRequest();
        Close();
    }

    private void cancel_Click(object sender, EventArgs e)
    {
        Close();
    }
}
