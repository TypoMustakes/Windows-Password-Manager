﻿namespace GUI;

sealed partial class MainForm : Form
{
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        SearchBox = new TextBox();
        GeneratePassword = new Button();
        ResultList = new PasswordListBox(PathRequest);
        DecryptBtn = new Button();
        Cancel = new Button();
        SuspendLayout();
        // 
        // SearchBox
        // 
        SearchBox.Location = new Point(10, 20);
        SearchBox.Margin = new Padding(3, 2, 3, 2);
        SearchBox.Name = "SearchBox";
        SearchBox.PlaceholderText = "Search for a password";
        SearchBox.Size = new Size(225, 23);
        SearchBox.TabIndex = 0;
        SearchBox.TextChanged += ReloadResults;
        // 
        // GeneratePassword
        // 
        GeneratePassword.Location = new Point(241, 20);
        GeneratePassword.Name = "GeneratePassword";
        GeneratePassword.Size = new Size(75, 23);
        GeneratePassword.TabIndex = 5;
        GeneratePassword.Text = "Generate";
        GeneratePassword.UseVisualStyleBackColor = true;
        GeneratePassword.Click += OpenPasswordGenerator;
        // 
        // ResultList
        // 
        ResultList.ColumnWidth = 20;
        ResultList.FormattingEnabled = true;
        ResultList.HorizontalScrollbar = true;
        ResultList.ItemHeight = 15;
        ResultList.Location = new Point(10, 48);
        ResultList.Name = "ResultList";
        ResultList.Size = new Size(306, 274);
        ResultList.TabIndex = 6;
        // 
        // DecryptBtn
        // 
        DecryptBtn.Location = new Point(241, 328);
        DecryptBtn.Name = "DecryptBtn";
        DecryptBtn.Size = new Size(75, 23);
        DecryptBtn.TabIndex = 7;
        DecryptBtn.Text = "Decrypt";
        DecryptBtn.UseVisualStyleBackColor = true;
        DecryptBtn.Click += Decrypt;
        // 
        // Cancel
        // 
        Cancel.Location = new Point(160, 328);
        Cancel.Name = "Cancel";
        Cancel.Size = new Size(75, 23);
        Cancel.TabIndex = 8;
        Cancel.Text = "Cancel";
        Cancel.UseVisualStyleBackColor = true;
        Cancel.Click += CancelPressed;
        // 
        // MainForm
        // 
        AcceptButton = DecryptBtn;
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(324, 374);
        Controls.Add(Cancel);
        Controls.Add(DecryptBtn);
        Controls.Add(ResultList);
        Controls.Add(GeneratePassword);
        Controls.Add(SearchBox);
        Margin = new Padding(3, 2, 3, 2);
        Name = "MainForm";
        Text = "Password Manager";
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion

    private TextBox SearchBox;
    private Button GeneratePassword;
    private PasswordListBox ResultList;
    private Button DecryptBtn;
    private Button Cancel;
}