﻿using System.Text;

namespace Common;
public static class PasswordGenerator
{
    public static Error? ExceptionOccured;
	public static int DEFAULT_LENGTH = 16;
    private static string RandomStr(int length = 16, bool no_symbols = false)
    {
        StringBuilder builder = new StringBuilder();
        Random rnd = new Random();
        for (int i = 0; i < length; i++)
        {
            if (rnd.Next(0, 101) <= 60) //60% chance for a character
            {
                char c = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * rnd.NextDouble() + 65))); //random character
                if (no_symbols && Char.IsSymbol(c))
                {
                    while (Char.IsSymbol(c))
                    {
                        c = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * rnd.NextDouble() + 65))); //random character
                    }
                }
                builder.Append(c);
            }
            else //40% change for number
            {
                builder.Append(Convert.ToChar(rnd.Next(0, 10)));
            }
        }

        return builder.ToString();
    }

    public static string? New(string recipient, int length, bool no_symbols = false)
    {
        ProcessBuilder pb = new ProcessBuilder();
        pb.ProcessFailed += (e) => ExceptionOccured?.Invoke(e);

		//	if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
		//	{
		return pb.GetOutput("cmd.exe", $"echo {RandomStr(length, no_symbols)} | gpg --quiet --encrypt --recipient {recipient}");
		//	}
		//	else
		//	{
		//		return pb.GetOutput("echo", $"{RandomStr(length, no_symbols)} | gpg --quiet --encrypt --recipient {recipient}");
		//	}
    }
}
