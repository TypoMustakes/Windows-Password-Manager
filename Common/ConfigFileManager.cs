﻿using Microsoft.VisualBasic.FileIO;

namespace Common;

public delegate void Error(Exception e);
public static class ConfigFileManager
{
#region Config flags
	/*Configuration: (format: fieldname=value)

	  password_store_path: path to the folder which contains the .gpg files
	  recipient: whose public key to use for encryption when a new password is created
	*/
	private static string CONFIGPATH = SpecialDirectories.CurrentUserApplicationData + "\\config.cfg"; //TODO: make this more unix-friendly
	private static char DELIMETER = '=';
	private static string RECIPIENT = "recipient";
	private static string PATH = "password_store_path";
#endregion

	private static Config Configuration;
    public static event Error? ExceptionOccured;

	public static void Init()
	{
        bool success = false;

        while (!success)
        {
            if (File.Exists(CONFIGPATH))
            {
                StreamReader sr = new StreamReader(CONFIGPATH);
                string? path = null, recipient = null;
                while (!sr.EndOfStream)
                {
                    string[]? fields = sr.ReadLine().Split(DELIMETER);
                    if (fields != null)
                    {
                        if (fields[0] == PATH)
                        {
                            path = fields[1];
                        }
                        else if (fields[0] == RECIPIENT)
                        {
                            recipient = fields[1];
                        }
                    }
                    else //probably an empty line or something
                    {
                        continue;
                    }
                }

                if (path != null && recipient != null)
                {
                    Configuration = new Config(path, recipient);
                    success = true;
                }
                else
                {
                    throw new InvalidConfigurationException("One or more required fields were missing from the configuration file.");
                }
            }
            else
            {
                CreateDefaultConfig();
            }
        }
	}

    public static string GetPath()
    {
		return Configuration.PasswordStorePath;
    }

	public static string GetRecipient()
	{
		return Configuration.Recipient;
	}

    private static void CreateDefaultConfig()
    {
        try
        {
            string user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            StreamWriter sw = new StreamWriter(CONFIGPATH);
            sw.WriteLine(RECIPIENT + DELIMETER + user);
            sw.WriteLine(PATH + DELIMETER + SpecialDirectories.CurrentUserApplicationData); //TODO: make this unix-friendly
            sw.Close();
        }
        catch (Exception e)
        {
            ExceptionOccured?.Invoke(e);
        }
    }
}
