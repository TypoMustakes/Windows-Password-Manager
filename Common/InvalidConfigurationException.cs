namespace Common;

class InvalidConfigurationException : Exception
{
	public InvalidConfigurationException() : base() { }
	public InvalidConfigurationException(string message) : base(message) { }
	public InvalidConfigurationException(string message, Exception inner) : base(message, inner) { }
}
