namespace Common;

class Config
{
	public string PasswordStorePath { get; set; }
	public string Recipient { get; set; }

	public Config(string PasswordStorePath, string Recipient)
	{
		this.PasswordStorePath = PasswordStorePath;
		this.Recipient = Recipient;
	}
}
