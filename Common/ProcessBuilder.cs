using System.Diagnostics;
using System.Text;

namespace Common;

public delegate void ProcessSuccess();
public delegate void ProcessFailure(Exception e);
public sealed class ProcessBuilder
{
	public event ProcessSuccess? ProcessFinished;
	public event ProcessFailure? ProcessFailed;

	public void Run(string procName, string args)
	{
		try
		{
			Process.Start(procName, args);
			ProcessFinished?.Invoke();
		}
		catch (Exception e)
		{
			ProcessFailed?.Invoke(e);
		}
	}
	
	public string? GetOutput(string procName, string args)
	{
		try
		{
			Process proc = new Process()
			{
				StartInfo = new ProcessStartInfo
				{
					FileName = procName,
					Arguments = args,
					UseShellExecute = false,
					RedirectStandardOutput = true,
					CreateNoWindow = true
				}
			};

			proc.Start();

			StringBuilder builder = new StringBuilder();
			while (!proc.StandardOutput.EndOfStream)
			{
				builder.Append(proc.StandardOutput.ReadLine());
			}

			ProcessFinished?.Invoke();
			return builder.ToString();
		} catch (Exception e)
		{
			ProcessFailed?.Invoke(e);
		}

		return null;
	}
}
