﻿using Common;
namespace CLI;

class Program
{
	static string BINARY_NAME = "pass";
	
	static int Main(string[] args)
	{
		ConfigFileManager.ExceptionOccured += (e) => ErrorOccured(e);
		ConfigFileManager.Init();

		if (args.Length == 0)
		{
			ListPasswords();
			return 0;
		}
		else
		{
			switch (args[0])
			{
				case "help":
					HelpMessage();
					break;
				case "list":
					if (ListPasswords() == 1)
					{
						return 1;
					}
					break;
				case "new":
					if (New() == 1)
					{
						return 1;
					}
					break;
				default:
					HelpMessage();
					break;
			}
		}

		return 0;
	}

	private static int Generate(string name, string length = PasswordGenerator.DEFAULT_LENGTH.ToString(), string symbols = 'true')
	{
		try
		{
			//do not save output, just check if exceptions are thrown
			Convert.ToInt32(length);
			Convert.ToBoolean(symbols);

			PasswordGenerator.ExceptionOccured += (e) => ErrorOccured(e, "There was a problem generating your password. See details");

			File.WriteAllText(
				$"{ConfigFileManager.GetPath()}\\{name}",
				PasswordGenerator.New(
					ConfigFileManager.GetRecipient(),
					length,
					symbols
				)
			)
			return 0;
		}
		catch (FormatException e)
		{
			ErrorOccured(e, "Please provide valid parameters:\n\t- length:\tnumber\n\t- symbols\ttrue|false)");
			return 1;
		}
		catch (IOException e)
		{
			ErrorOccured(e, "IO Error");
		}
	}

	static int New()
	{
		switch (args.Length)
		{
			case 4:
				Generate(args[1], args[2], args[3]);
				break;
			case 3:
				//TODO: determine if the second flag is a bool or an int
				Generate(args[1], args[2]);
				break;
			case 2:
				Generate(args[1]);
				break;
			default:
				//either there's an unexpected argument, or not enough
				ErrorOccured(new ArgumentException(), $"Invalid or missing arguments, see '{BINARY_NAME} help'");
				return 1;
				break;
		}
		
		return 0;
	}

	static int ListPasswords()
	{
		DirectoryInfo d;
		FileInfo[] files = new FileInfo[0];
        try
        {
			//since this file is already dependent on the Common namespace, we don't need events to mitigate this; better off asking ConfigFileManager for the path directly
            d = new DirectoryInfo(ConfigFileManager.GetPath());
            files = d.GetFiles("*.gpg");
        }
        catch (ArgumentNullException e)
        {
			ErrorOccured(e, "Error while getting password files");
			return 1;
        }

		foreach (FileInfo f in files)
		{
			Console.WriteLine($"\t{f.Name}");
		}

		return 0;
	}

	static void ErrorOccured(Exception e, string errorName)
	{
		Console.WriteLine($"{errorName}: {e}\nMore details:\n{e.ToString()}");
	}

	static void ErrorOccured(Exception e)
	{
		Console.WriteLine($"Unexpected error: {e}\nMore details:\n{e.ToString()}");
	}
	
	static void HelpMessage()
	{
		Console.WriteLine($"Usage: {BINARY_NAME} <arguments> [optional arguments]");
		Console.WriteLine("\nArguments:");
		Console.WriteLine("\t- list:\t\t\tlist all passwords; this is the default action");
		Console.WriteLine("\t- <password>:\t\tdecrypt password and copy it to clipboard");
		Console.WriteLine($"\t- new <name>:\t\tgenerate password\n\t\t[length]:\tthe length of the password, default is {PasswordGenerator.DEFAULT_LENGTH}\n\t\t[true|false]:\twhether the password should include symbols, default is true");
		Console.WriteLine($"\t- rm <password>:\tdelete password");
		Console.WriteLine($"\t- insert <password>:\tinstead of generating the password, you enter its contents");
		Console.WriteLine($"\t- find <search query>:\tlist password that match your query");
		Console.WriteLine($"\t- edit <password>:\tedit the contents of an existing password");
	}
}
